package emailutility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;


public class MainProgram 
{
	static MongoClient client;
	static DB crawler;
	static DBCollection sampleCrawlData; 
	
	static
	{
		client=new MongoClient("178.32.49.5",27017);
		crawler=client.getDB("crawler");
		sampleCrawlData=crawler.getCollection("sampleCrawldata");
	}
	
	public static void main(String args[])
	{
		/* DB Query
		 * db.getCollection('crawldata').aggregate( 
         		[
             		{ $match : { systemDate : "2016-11-25" } },
             		{$group : { _id : "$subCategory" ,  moduleName :{ $first : "$moduleName" },  total : {  $sum: 1 }}}
         		]
     	   )
		 */
		
		//String systemDate=new SimpleDateFormat("yyyy-MM-dd").format(new Date()); 
		String systemDate="2016-12-20";
		BasicDBObject match=new BasicDBObject("$match", new BasicDBObject("systemDate", systemDate).append("status", "false"));
		System.out.println(match.toString());
		BasicDBObject group=new BasicDBObject("$group", new BasicDBObject( "_id", "$subCategory").append("moduleName", new BasicDBObject("$first", "$moduleName")).append("total", new BasicDBObject("$sum",1)));
		System.out.println(group.toString());
		
		AggregationOutput output = sampleCrawlData.aggregate(match, group);
		
		String Buyers="";
		String News="";
		String Network="";
		String Tools="";
		
		int totalCount=0;
		long emailCount=0;
		long phoneCount=0;
		
		for(DBObject obj:output.results())
		{
			switch(obj.get("moduleName").toString())
			{
				case "Buyers"  : Buyers+="\t"+obj.get("_id")+" = "+obj.get("total")+"\n";  break;
				case "News"    : News+="\t"+obj.get("_id")+" = "+obj.get("total")+"\n";  break;
				case "Network" : Network+="\t"+obj.get("_id")+" = "+obj.get("total")+"\n";  break;
				case "Tools"   : Tools+="\t"+obj.get("_id")+" = "+obj.get("total")+"\n";  break;
			}		
			
			totalCount+=Integer.parseInt(obj.get("total").toString());
		}
		
		// Tools=6771  Books=5065  Communities=4037  Investor=3007  Apps=0
		long investors=sampleCrawlData.count(new BasicDBObject("subCategory", "Investors"));;
		long tools=sampleCrawlData.count(new BasicDBObject("subCategory", "Tools"));
		long apps=sampleCrawlData.count(new BasicDBObject("subCategory", "Apps"));
		long appsData=sampleCrawlData.count(new BasicDBObject("subCategory", "AppsData"));
		long communities=sampleCrawlData.count(new BasicDBObject("subCategory", "OnlineCommunities"));
		long sellers =sampleCrawlData.count(new BasicDBObject("subCategory", "Sellers"));
		//long books=sampleCrawlData.count(new BasicDBObject("subCategory", "Books"));
		
		//{status:"true","expiredStatus" : "live","systemDate" : "2016-12-12","companyEmail":{$exists:true,$ne:""}}
		emailCount=sampleCrawlData.count(new BasicDBObject("status", "true").append("expiredStatus", "live").append("systemDate", systemDate).append("moduleName","Buyers").append("companyEmail", new BasicDBObject("$exists",true).append("$ne", "")));
		phoneCount=sampleCrawlData.count(new BasicDBObject("status", "true").append("expiredStatus", "live").append("systemDate", systemDate).append("moduleName","Buyers").append("companyNumber", new BasicDBObject("$exists",true).append("$ne", "")));
		
		
		String message="\n\nBuyers :\n"+Buyers+"\n\nNews :\n"+News+"\n\nNetwork :\n"+Network+"\n\nTools :\n"+Tools+"\n\nTotal Count = "+totalCount+"\n\n**********************************************************\n\n";
		message+="\nTodays Buyers :\n\tEmail Count= "+emailCount+"\n\tPhone Number Count= "+phoneCount+"\n\n**********************************************************\n\n";
		
		message+="Static Data :\n\n\tInvestor = "+investors+"\n\tTools = "+tools+"\n\tApps = "+apps+"\n\tAppsData = "+appsData+"\n\tOnlineCommunities = "+communities+"\n\tSellers = "+sellers;
		
		System.out.println(message);
		
		try
		{
			ReadPropertiesFile.readConfig();
			//System.out.println(ReadPropertiesFile.setFrom+"="+ReadPropertiesFile.setPassword);
			GmailServer sender = new GmailServer(ReadPropertiesFile.setFrom, ReadPropertiesFile.setPassword);
		
			String body = "Hello,\n\n\tAll SubCategory Count for Date : "+systemDate+" \n\n"+message; //mail body starts here
			sender.sendMail(systemDate+" : ModuleName-SubCategoy Count, Email Utility",body,ReadPropertiesFile.setFrom,ReadPropertiesFile.emailTO); //send mail here
			System.out.println("Email Sent Successfully..."); 
		}
		catch(Exception e)
		{
			System.out.println("Error Sending Email");
			e.printStackTrace();
		}		
	}
}
