package emailutility;

import java.io.FileInputStream;
import java.util.Properties;
 
public class ReadPropertiesFile
{
	public static String delay;
	public static String timetoquery;
	public static String setFrom;
	public static String setPassword;
	public static String emailTO;
	
	public  static void readConfig() throws Exception
	{
		try
		{
 
		    Properties pro = new Properties();
		    String path = System.getProperty("user.dir")+"/emailSent_Props.properties";
		    pro.load(new FileInputStream(path));	   
 
		    delay = pro.getProperty("delay");
		    timetoquery = pro.getProperty("timetoquery");
		    setFrom = pro.getProperty("setFrom");
		    setPassword = pro.getProperty("setPassword");
		    emailTO = pro.getProperty("emailTO");	  		   
 
		}
		catch(Exception e)
		{
            throw new Exception(e);
		}
 
	}
 
}